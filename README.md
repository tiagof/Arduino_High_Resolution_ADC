## Introduction
Ardu_24Bit_ADC_V3  
Arduino 24Bit ADC with TI ADS1248.  
Autors: Peter Nyffeler & Tiago Neves.  
This firmware is an interface to control an ADC(ADS1248) connected to an Arduino.  
The ADC is to be controlled using Serial Port commands, Seial Baud is 57600.  
This sketch is compiling to:
- Arduino Uno R3

### Installing The libraries
Change the Project location in the arduino application:
- "File"->"Preferences"->"Sketchbook location:"
- Should look like: /home/user/Arduino_High_Resolution_ADC

## Command Structure

### Set Commands (return "T" for true and "F" for false) 

- SC=set input Channel, return  & Gain.
numeric entry in#, ret#, Gain (1,2,4,8,64,128) seperated by "," e.g. SC0,4,64

- SI=set interfall for continuous measurments and set adc appropriate sample rate
numeric floatingpoint value in seconds 0.01s<=4s  
REQUIERS leading zero for values <1s !

- SL=set List channel parameters.
numeric entry channel#, in#, ret#, gain eg SL3,4,0,2 to disable a channel set in&ret = 0 eg SL4,0,0,1

- SR=set Register (numeric entry register adress, value) 

- SS= set Sampling rate.  
CAUTION! will overwrite rate calculated by SI command.  
allowed SPS (Sampling rate per Second) values:
- 5 
- 10
- 20 (default)
- 40
- 80
- 160
- 320
- 640
- 1000
- 2000

### Get Commands

- GB=value of Selected channel (returns binary Value of channel set with SC command)
- GV=value of selected channel in volts
- GC=get Continuous selected channel:
 - b=binary
 - v=volt
send continuous value of channel set with SC command until any character was send.
NOTE: character is not part of a new command!
GL=values of channel List:
- b=binary
- v=volt
GR=read Register (numeric entry adress of register to be read)

GM=read system Monitor (numeric entry)
- 1 = Offset measurement
- 2 = Gain measurement
- 3 = Temperature diode
- 4 = External REF1 measurement (ADS1248 only)
- 5 = External REF0 measurement
- 6 = AVDD measurement
- 7 = DVDD measurement

## Register Map (for details see ADS1248 data sheet)

- 00h  MUX0
- 01h  VBIAS
- 02h  MUX1
- 03h  SYS0
- 04h  OFC0
- 05h  OFC1
- 06h  OFC2
- 07h  FSC0
- 08h  FSC1
- 09h  FSC2
- 0Ah  IDAC0
- 0Bh  IDAC1
- 0Ch  GPIOCFG
- 0Dh  GPIODIR
- 0Eh  GPIODAT
GP=get Parameters (return all ADC settings)

## Example to continuous read Channel 1, 0.1s intervall
sc1,0,1
si0.1
gcv get continuous voltage value.

## TODO
Identify problem with Parser sending inverted values.

sr10,15  set register IDAC0 to 1111 (DRY and 1,5mA) or other current 100uA (1 010 = 10)
sr15,10 (send this because of inverted parser)

sr11,16  Set Register with Map IDAC exitation current to AN1
sr16,11 (send this because of inverted parser)
