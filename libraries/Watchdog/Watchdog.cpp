/* Watchdog.ccp*/
/*
Class for 23bit+sign ADC ADS1248 from TI
>>> NOTE: currently only ONE OBJET of ADS1248 is possible since other ADS1248 would use the same ChipSelect, Start, DataReady pin and Interrupt Pin
Peter Nyffeler
June 2014
Livia Waller 
August 2016
*/

#include "Arduino.h"
#include <SPI.h>
#include <Watchdog.h>

//Commands
#define ADS1248_wakeup		0x00
#define ADS1248_sleep		0x02
#define ADS1248_sync		0x04
#define ADS1248_reset		0x06
#define ADS1248_nop			0xFF
#define ADS1248_rdonce		0x12		// Read data oncce
#define ADS1248_rdcont		0x14		// Read data continuously
#define ADS1248_stopcont	0x16		// Stop reading data continuouusly
#define ADS1248_rreg		0x20		// To 1st byte add 1st register to read and to 2nd byte nummber-1 of bytes to read
#define ADS1248_wreg		0x40		// Add register to 1st byte and value to 2nd
#define ADS1248_offesetcal	0x60
#define ADS1248_gaincal		0x61
#define ADS1248_selfoffcal	0x62

// Register
#define ADS1248_MUX0		0x00		// Multiplecer Ctrl 0 / BCS, MUX_SP, MIUX_SN
#define ADS1248_VBIAS		0x01		// Bias Voltage / VBIAS
#define ADS1248_MUX1		0x02		// Multiplexer Control /CLKSTAT, VREFCON, VREFSEL, MUXCAL
#define ADS1248_SYS0		0x03		// System Control Reg / PGAS, DOR (Data output rate)  
#define ADS1248_OFC0		0x04		// Offest Cal Cooeff 0
#define ADS1248_OFC1		0x05		// Offest Cal Cooeff 1
#define ADS1248_OFC2		0x06		// Offest Cal Cooeff 2
#define ADS1248_FSC0		0x07		// Full Scale Cal 0
#define ADS1248_FSC1		0x08		// Full Scale Cal 1
#define ADS1248_FSC2		0x09		// Full Scale Cal 2
#define ADS1248_IDAC0		0x0A		// I_DAC Reg2 /ID, DRDY, IMAF
#define ADS1248_IDAC1		0x0B		// I_DAC Reg1 /I1DIR, I2DIR
#define ADS1248_GPIOCFG		0x0C		// GeneralPoupose IO Conf 
#define ADS1248_GPIODIR		0x0D		// GeneralPoupose IO Direction
#define ADS1248_GPIODAT		0x0E		// GeneralPoupose IO Data
 
// *** Register Constants ***
// for MUX0  
// Analog + Channel (Bits 5:3)					
#define ADS1248_I0			0x00
#define ADS1248_I1			0x08
#define ADS1248_I2			0x10
#define ADS1248_I3			0x18
#define ADS1248_I4			0x20
#define ADS1248_I5			0x28
#define ADS1248_I6			0x30
#define ADS1248_I7			0x38
// Analog return Channel (Bits 2:0) + I_DAC routing (IDAC1 reg bits 3:0)
#define ADS1248_Ir0			0x00
#define ADS1248_Ir1			0x01
#define ADS1248_Ir2			0x02
#define ADS1248_Ir3			0x03
#define ADS1248_Ir4			0x04
#define ADS1248_Ir5			0x05
#define ADS1248_Ir6			0x06
#define ADS1248_Ir7			0x07
// Burnout Isrc (Bits 7:6)
#define BCSoff				0x00		//off
#define BCS05				0x40		//0.5uA
#define BCS2				0x80		// 2uA
#define BCS10				0xC0		//10uA
// I_DAC additional routing	(IDAC1 reg bits 3:0)
#define ADS1248_IEXT1		0x08
#define ADS1248_IEXT2		0x09
#define ADS1248_discon		0x0C
//for MUX1
// CLKSTAT (Clock Status) Bit 7 		READONLY 0x00=CLKint 0x80=CLKext 
// VREFCON (Vref ctrl)	Bits 6:5
#define VREFoff				0x00
#define VREFon				0x20
#define VREFtemp			0x40
// REFSEL (Ref for ADC) Bits4:3
#define REF0				0x00
#define REF1				0x08
#define REFint				0x10
#define REFint2REF0			0x18
// MuxCal Bit 2:0 (System monitor parameter)
#define MUXCALnorm			0x00		
#define MUXCALoffset		0x01
#define MUXCALgain			0x02
#define MUXCALtemp			0x03
#define MUXCALref1			0x04
#define MUXCALref0			0x05
#define MUXCALavdd			0x06
#define MUXCALdvdd			0x07
//for SYS0
// PGA Bit 6:4
#define GAIN1				0x00
#define GAIN2				0x10
#define GAIN4				0x20
#define GAIN8				0x30
#define GAIN16				0x40
#define GAIN32				0x50
#define GAIN64				0x60
#define GAIN128				0x70
// DOR (Data output rate) Bit 3:0
#define SPS5				0x00
#define SPS10				0x01
#define SPS20				0x02
#define SPS40				0x03
#define SPS80				0x04
#define SPS160				0x05
#define SPS320				0x06
#define SPS640				0x07
#define SPS1000				0x08
#define SPS2000				0x09
//for IDSAC0
// DRDY MODE (Pin DOUT mode) Bit 3
#define DOUTonly			0x00
#define DOUTandREADY		0x08	//Dota out and Data Ready
//IMAG2 (I_DAC in uA) Bit2:0
#define	IDACoff 			0x00
#define	IDAC50				0x01
#define	IDAC100				0x02
#define	IDAC250				0x03
#define	IDAC500				0x04
#define	IDAC750				0x05
#define	IDAC1000			0x06
#define	IDAC1500			0x07
//for IDAC1
// Output of IDAC1 to input Bits 7:4)
#define DAC1_I0				0x00
#define DAC1_I1				0x10
#define DAC1_I2				0x20ADS1248_rdonce
#define DAC1_I3				0x30
#define DAC1_I4				0x40
#define DAC1_I5				0x50
#define DAC1_I6				0x60
#define DAC1_I7				0x70
#define DAC1_IEXT1			0x80
#define DAC1_IEXT2			0x90
#define DAC1_discon			0xC0
// Output of IDAC2 to input Bits 3:0)
#define DAC2_I0				0x00
#define DAC2_I1				0x01
#define DAC2_I2				0x02
#define DAC2_I3				0x03
#define DAC2_I4				0x04
#define DAC2_I5				0x05
#define DAC2_I6				0x06
#define DAC2_I7				0x07
#define DAC2_IEXT1			0x08
#define DAC2_IEXT2			0x09
#define DAC2_discon			0x0C
//Watchdog LED Pin4 Digital
#define WDT_LED				0x04

//*** CLASS ADS1248 ***
/******************************************************************************
 * Constructor (void)
 * Overview:        Initializes SPI interface, CS and Start Pin as output,
 *****************************************************************************/
ADS1248::ADS1248(void)
{
//set HS-lines in idle state
  pinMode(SS,OUTPUT);
  pinMode(ADS1248_start,OUTPUT);
  ADS1248en();
  // initialize SPI
  SPI.begin();
  SPI.setClockDivider(SPI_CLOCK_DIV8);
  //SPI.setClockDivider(SPI_CLOCK_DIV16); //1MHz SPIclk
  SPI.setDataMode(SPI_MODE1);
  //define default configuration
  cur_MUX0  = 0x08;			//ADS1248_I1|ADS1248_Ir0
  cur_VBIAS = 0x00;
  cur_MUX1  = 0x30;			//Vref_always on|VREFon|REFint2REF0
  cur_SYS0  = 0x70;			//GAIN128|SPS5
  //init ADS1248
  ADS1248en();
  //load default configuration
  SPI.transfer(ADS1248_wreg|ADS1248_MUX0); 	//write to register starting with MUX0
  SPI.transfer(14); 						//and the all following registers
  SPI.transfer(cur_MUX0);		//MUX0	(ADS1248_I1|ADS1248_Ir0)
  SPI.transfer(cur_VBIAS);		//VBIAS	(0x00)
  SPI.transfer(cur_MUX1);		//MUX1	(Vref_always on|VREFon|REFint2REF0)
  SPI.transfer(cur_SYS0);		//SYS  	(gain=128, sps=5 for best self offset calibration)
  SPI.transfer(0x00);			//OFC0
  SPI.transfer(0x00);			//OFC1
  SPI.transfer(0x00);			//OFC2
  SPI.transfer(0x00);			//FSC0
  SPI.transfer(0x00);			//FSC1
  SPI.transfer(0x00);			//FSC2
  SPI.transfer(DOUTandREADY);	//IDAC0
  SPI.transfer(0x00);			//IDAC0
  SPI.transfer(0x00);			//IDAC1
  SPI.transfer(0x00);			//GPIOCFG
  SPI.transfer(0x00);			//GPIODIR
  SPI.transfer(0x00);			//GPIODAT
  SPI.transfer(ADS1248_selfoffcal); //do offset self calibration
  while (digitalRead(ADS1248_drdy)){}; //wait for calibration competed
  ADS1248dis();
  //calculate resolution for gain corrected binary value
  resolution = /*Vref*/ 2.048/0x7fffffff;
  //clear AD_value and CHListParam
  ADS1248val=0;
  for (i=0;i<6;i++)
  {
    CHListParam.L_MUX0[i] = 0;
    CHListParam.L_SYS0[i] = 0;
	CHListParam.L_shift4gain[i] = 7;
  }
}

//+++++++++++++++++++++++
//+++ private Methods +++
//+++++++++++++++++++++++

//******************************************************************************
void ADS1248::ReadVal(void)
{
  ADS1248val=0;
  for (i=0;i<3;i++)
 	{
      ADS1248val = ADS1248val << 8;
	  ADS1248val = ADS1248val + SPI.transfer(ADS1248_nop);
	}
  if (ADS1248val>0x7fffff)ADS1248val=ADS1248val+0xff000000;	//24bit signed to 32bit signed
  ADS1248val = ADS1248val << shift4gain;
}

//++++++++++++++++++++++
//+++ public Methods +++
//++++++++++++++++++++++

/******************************************************************************
 * SetCH (int CHpos, int CHret, int GAIN) NOTE:GAIN must be 2^n & <= 128
 * Overview:       set input channels (+ & -) and gain
 *****************************************************************************/
boolean ADS1248::SetCH (int GAIN, int CHret, int CHpos)
{
  if (CHpos == CHret)return (false);
  if (CHpos > 7)return (false);
  if (CHret > 7)return (false);
  if (GAIN > 128)return (false);
  cur_MUX0 = CHpos<<3 | CHret;
  i=0;
  while ((1<<i != GAIN) & (i <= 0x07)) i++;
  if (i > 7)return (false);
  shift4gain = 8-i;  
  ADS1248en();
  cur_SYS0  = (cur_SYS0 & 0x0F) | (i<<4);
  SPI.transfer(ADS1248_wreg|ADS1248_MUX0); 	//write register MUX0
  SPI.transfer(3); 			 				//and the following 3 registers
  SPI.transfer(cur_MUX0);
  SPI.transfer(cur_VBIAS);
  SPI.transfer(cur_MUX1);
  SPI.transfer(cur_SYS0);
  ADS1248dis();
  return (true);
}

/******************************************************************************
 * SetSamplingRate (int rate) NOTE: SAMPLING RATE must be a value as in datasheet
 * Overview:       set samplingrate per sec
 *****************************************************************************/
boolean ADS1248::SetSamplingRate (int rate)
{
  int x=5;
  i=0;
  while ((x != rate) & (i <10))
  {
    x = x * 2;
	i++;
  }
  if (rate == 1000) i = 8;
  if (rate == 2000) i = 9;
  if (i > 9) return (false);
  ADS1248en();
  cur_SYS0  = (cur_SYS0 & 0xF0) | i;
  SPI.transfer(ADS1248_wreg|ADS1248_SYS0); 	//write register MUX0
  SPI.transfer(0); 			 				//only this one
  SPI.transfer(cur_SYS0);
  ADS1248dis();
  //update CHListParam for new sampling rate
  for (i=0;i<6;i++) CHListParam.L_SYS0[i] = (CHListParam.L_SYS0[i] & 0xF0) | i;
  return (true);
}

/******************************************************************************
 * GetVal (void)
 * Overview:       returns AD VAlue (binary)
 *****************************************************************************/
long ADS1248::GetVal (void)
{
  digitalWrite(SS, LOW);  //enable CS ADS1248;   
  PCMSK0 = 0x01; //enable change on PCINT0 (Arduino pin 8) to set flag PCIF0 in in pin_change_int. flag register PCIF 
  digitalWrite(ADS1248_start, HIGH); //start conversion
  digitalWrite(ADS1248_start, LOW); 	   
  do
  {
    PCIFR = 0xff;	     	 //clear pin_change_int. flag register
    while(!PCIFR & 0x01){};  //wait on change of DRDY
  }
  while(digitalRead(8) == HIGH); //!DRDY
  PCMSK0 = 0x0; //disable change on PCINT0 (Arduino pin 8) to set flag PCIF0 in in pin_change_int. flag register PCIF   
  ReadVal();
  ADS1248dis();
  return(ADS1248val);
}

/******************************************************************************
 * GetVolt (void)
 * Overview:       returns AD VAlue in Volts
 *****************************************************************************/
 double ADS1248::GetVolt (void)
{
  GetVal();
  return (ADS1248val*resolution);
}

 /******************************************************************************
 * StartContinuous (void)
 * Overview:
 *****************************************************************************/
void ADS1248::StartContinuous (void)
{
  ADS1248en();
  //start conversion and wait for completed
  digitalWrite(ADS1248_start, HIGH);
  while (digitalRead(ADS1248_drdy)){}; //wait for conversion competed
  SPI.transfer(ADS1248_rdcont);
  }
  
/******************************************************************************
 * GetContiunous (void)
 * Overview:       returns AD VAlue at sampling rate
 *****************************************************************************/
long ADS1248::GetContinuous (void)
{
  while (digitalRead(ADS1248_drdy)){}; //wait for conversion competed
  ReadVal();
  return(ADS1248val);
}

/******************************************************************************
 * GetContiunous_volt (void)
 * Overview:       returns AD VAlue at sampling rate in volts
 *****************************************************************************/
double ADS1248::GetContinuous_volt (void)
{
  GetContinuous();
  return(ADS1248val*resolution);
}

 /******************************************************************************
 * StopContinous (void)
 * Overview:
 *****************************************************************************/
 void ADS1248::StopContinuous (void)
{
  while (digitalRead(ADS1248_drdy)){}; //wait for conversion competed
  SPI.transfer(ADS1248_stopcont);
  digitalWrite(ADS1248_start, LOW);
  ADS1248dis();
}

/******************************************************************************
 * boolean SetListCH (int CH, int CHpos, int CHret, int GAIN)
 * Overview:		Defines input lines for up to 6 channels SE
 *****************************************************************************/
 boolean ADS1248::SetListCH (int CH, int CHpos, int CHret, int GAIN)
 {
  if ((CH<0) || (CH>6))return (false);
  if ((CHpos == CHret) && (CHpos != 0)) return (false);
  if ((CHpos<0) || (CHpos > 7))return (false);
  if ((CHret<0) || (CHret > 7))return (false);
  if (GAIN > 128)return (false);
  i=0;
  while ((1<<i != GAIN) && (i <= 0x07)) i++;
  if (i > 7)return (false);
  CHListParam.L_MUX0[CH] = CHpos<<3 | CHret;
  CHListParam.L_SYS0[CH] = (cur_SYS0 & 0x0F) | (i<<4);
  CHListParam.L_shift4gain[CH] = 8-i;
  return (true);
 }

 /******************************************************************************
 * ScanCH
 * Overview:      	Scan the AD channels set with SetListCH
 *****************************************************************************/
 boolean ADS1248::ScanCH (void)
 {
  int n;
  
  n=0;
  i=0;
  while (( CHListParam.L_MUX0[i] == 0) && (i<7)) //find 1st channel in list to read
  {
   CH_VAL[i] = 0; 					//clear value of unused channel
CH_VAL[i] = i; 					//clear value of unused channel
   i++;
  };
  if (i==7) return (false);					//there is no channel to read
  ADS1248en();
  //select parameters for 1st channel to read
  SPI.transfer(ADS1248_wreg|ADS1248_MUX0); 	//set inputs to be used for 1st conversion
  SPI.transfer(0); 			 				
  SPI.transfer(CHListParam.L_MUX0[i]);
  SPI.transfer(ADS1248_wreg|ADS1248_SYS0);	//set new gain in register SYS0		
  SPI.transfer(0);														
  SPI.transfer(CHListParam.L_SYS0[i]);		
  //remind current channel + gain setting
  n=i;
  cur_SYS0  = (CHListParam.L_SYS0[i]);
  while (i<6)	//while there are other channels to read
  {
    i++;
    if (CHListParam.L_MUX0[i] != 0) //find next channel in list to read
	{
      //use time for AD cenversion to remind current channel,gain setting and find next channel
      cur_SYS0  = (CHListParam.L_SYS0[i]);
      while (digitalRead(ADS1248_drdy)){}; 	//wait for conversion competed
	  {
 	    CH_VAL[n] = SPI.transfer(ADS1248_wreg|ADS1248_MUX0);				//duplex SPI communication, read value and send command to write into register ADS1248_MUX0
	    CH_VAL[n] = (CH_VAL[n] << 8) +SPI.transfer(0);						//duplex SPI communication, read value and send write only into this register
	    CH_VAL[n] = (CH_VAL[n] << 8) +SPI.transfer(CHListParam.L_MUX0[i]);	//duplex SPI communication, read value and send inputs to be used for next conversion
	    if (cur_SYS0 != SPI.transfer(CHListParam.L_SYS0[i])) 				//if different gain for next channel
	    {
	      SPI.transfer(ADS1248_wreg|ADS1248_SYS0);							//set new gain in register SYS0		
          SPI.transfer(0);														
          SPI.transfer(CHListParam.L_SYS0[i]);														
	    }
		if (CH_VAL[n]>0x7fffff )CH_VAL[n]=CH_VAL[n]+0xff000000; 			//24bit signed to 32bit signed
		CH_VAL[n] = CH_VAL[n] << CHListParam.L_shift4gain[n];				//
        cur_SYS0  = (CHListParam.L_SYS0[i]); 								//remind current channel + gain setting
	  }	
	  n=i;												
	}
	else  CH_VAL[i]=0;		//clear value of unused channel;
  } 	//end while i<6
  //read last channel
  shift4gain = CHListParam.L_shift4gain[n];
  while (digitalRead(ADS1248_drdy)){}; 	//wait for conversion competed
  ReadVal();
  CH_VAL[n] = ADS1248val;
  ADS1248dis();
  return (true);
 }
 
  /******************************************************************************
 * ScanCHVolt
 * Overview:      	Convert binary value of channel to volt
 *****************************************************************************/
 void ADS1248::CH2Volt (void)
{
  for (i=0;i<6;i++)
  {
	if (CH_VAL[i] != 0) CH_VOLT[i] = CH_VAL[i]* resolution;
	else CH_VOLT[i] = 0;
  }	
}

/******************************************************************************
 * WriteReg (unsigned char REG, unsigned char VAL)
 * Overview:
 *****************************************************************************/
void ADS1248::WriteReg (unsigned char REG, unsigned char VAL)
{
  ADS1248en();
  SPI.transfer(ADS1248_wreg|REG); 		//read register
  SPI.transfer(0); 						//only this one
  SPI.transfer(VAL);
  ADS1248dis();
}

/******************************************************************************
 * ReadReg (unsigned char reg)
 * Overview:       returns register content
 *****************************************************************************/
unsigned char ADS1248::ReadReg (unsigned char REG)
{
  unsigned char REGval;

  ADS1248en();
  SPI.transfer(ADS1248_rreg|REG); 		//read register
  SPI.transfer(0); 						//only this one
  REGval=SPI.transfer(ADS1248_nop);
  ADS1248dis();
  return (REGval);
}

/******************************************************************************
 * SysMon (unsigned char para)
 * Overview:        return value of MUXCAL
 *****************************************************************************/
long ADS1248::SysMon (unsigned char MONITOR)
{
  ADS1248en();
  //read to register to store MUX1 and SYS settings
  SPI.transfer(ADS1248_rreg|ADS1248_MUX1);
  SPI.transfer(0x01);
  temp_MUX1=SPI.transfer(ADS1248_nop);
  temp_SYS0=SPI.transfer(ADS1248_nop);
  //write to register command for MUX1 to read para
  SPI.transfer(ADS1248_wreg|ADS1248_MUX1);
  SPI.transfer(0x00);
  SPI.transfer(temp_MUX1|MONITOR);
  delay(1);
  //start conversion and wait for completed
  digitalWrite(ADS1248_start, HIGH);
  digitalWrite(ADS1248_start, LOW);
  while (digitalRead(ADS1248_drdy)){};
  SPI.transfer(ADS1248_rdonce);
  ReadVal();
  //restore setting of MUX1
  SPI.transfer(ADS1248_wreg|ADS1248_MUX1);
  SPI.transfer(0x01);
  SPI.transfer(temp_MUX1);
  SPI.transfer(temp_SYS0);
  ADS1248dis();
  return(ADS1248val);
}
