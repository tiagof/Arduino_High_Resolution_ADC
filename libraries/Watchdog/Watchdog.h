/* ADS1248.h*/
/*
Header File of Class for 23bit+sign ADC ADS1248 from TI
>>> NOTE: currently only ONE OBJET of ADS1248 is possible since other ADS1248 would use the same ChipSelect, Start, DataReady pin and Interrupt Pin
Peter Nyffeler
June 2014
*/
#ifndef ADS1248_h
#define ADS1248_h
  
#include "Arduino.h"
#include <SPI.h>

/* 
defined by "pins_arduino.h" indluded in SPI.cpp
SS		10 //=CS
MOSI	11
MISO	12
SCK		13
*/

// ADS1248 pins for start and data-ready
#define ADS1248_start		9
#define ADS1248_drdy		8 	//if DRDY pin used
//#define ADS1248_drdy		12 	//if dedected via MISO 


struct ADS1248_CHListParamRec
{
	unsigned char L_MUX0[6];
	unsigned char L_SYS0[6];
	unsigned char L_shift4gain[6];
};

class ADS1248              
//************
//>>> NOTE: currently only ONE OBJET of ADS1248 is possible since other ADS1248 would use the same ChipSelect, Start and DataReady pin
{
  private:
  //******
	//variables
    int i;
	int cur_GAIN;
	unsigned char cur_MUX0; 
	unsigned char cur_VBIAS;
	unsigned char cur_MUX1;
	unsigned char temp_MUX1;
	unsigned char cur_SYS0;
	unsigned char temp_SYS0;
	unsigned char shift4gain;
	
	//ADS1248_CHListParamRec CHListParam;
	long ADS1248val;
	double resolution;
	
	//methods
	
	//ADS1248_DataIsReady (INT0_vect);
	
	inline static void ADS1248en (void);
	inline static void ADS1248dis (void);
	void ReadVal(void);

  public:
  //*****
    long  CH_VAL[6];
    double CH_VOLT[6];
	
   ADS1248_CHListParamRec CHListParam;
	
//Constructor
//***********
    ADS1248(void);
//Methods
//*******
	boolean  		SetCH (int CHpos, int CHret, int GAIN); //NOTE:GAIN must be a value of 2^n & <= 128
	boolean 		SetSamplingRate (int rate);
    long 			GetVal (void);
	double			GetVolt (void);
	void 			StartContinuous (void);
	long 			GetContinuous (void);
	double 			GetContinuous_volt (void);
	void 			StopContinuous (void);
	boolean 		SetListCH (int CH,int CHpos,int CHret,int GAIN); //NOTE: CH=channel of list (0-5)
	boolean 		ScanCH (void);
	void			CH2Volt (void);
    unsigned char 	ReadReg (unsigned char REG);
	void 			WriteReg (unsigned char REG, unsigned char VAL);
    long 			SysMon (unsigned char MONITOR);
};

//inline methods
//**************
void ADS1248::ADS1248en (void) 
{
  digitalWrite(SS, LOW);
  digitalWrite(ADS1248_start, HIGH);
}
void ADS1248::ADS1248dis (void)
{
 digitalWrite(SS, HIGH);
 digitalWrite(ADS1248_start, LOW);
}

#endif
