/*
Ardu_24Bit_ADC_V3
 Arduino 24Bit ADC with TI ADS1248
 Autor: Peter Nyffeler
 ________________________________________________________ 
 Command Structure
 =================
 Set Commands (return "T" for true and "F" for false) 
 -----------------
 SC=set input Channel, return  & Gain
 numeric entry in#, ret#, Gain (1,2,4,8,64,128) seperated by "," e.g. SC0,4,64
 -
 SI=set intervall for continious measurments and set adc appropriate sample rate
 numeric floatingpoint value in seconds 0.01s<=4s 
 REQUIERS leading zerro for values <1s !
 
 -
 SL=set List channel parameters
 numeric entry channel#, in#, ret#, gain eg SL3,4,0,2 to disable a channel set in&ret = 0 eg SL4,0,0,1
 -
 SR=set Register
 numeric entry register adress, value 
 -
 SS = set Sampling rate 
 CAUTION! will overwrite rate calculated by SI command
 allowed SPS (Sampling rate per Second) values
 5 
 10
 20 (default)
 40
 80
 160
 320
 640
 1000
 2000
 
 Get Commands
 ------------
 GB=value of Selected channel 
 returns binary Value of channel set with SC command
 -
 GV=value of selected channel in volts
 -
 GC=get Continuous selected channel
 b=binary
 v=volt
 send continuous value of channel set with SC command until any character was send.
 NOTE: character is not part of a new command!
 -
 GL=values of channel List
 b=binary
 v=volt
 -
 GR=read Register
 numeric entry adress of register to be read
 -
 GM=read system Monitor
 numeric entry
 1 = Offset measurement
 2 = Gain measurement
 3 = Temperature diode
 4 = External REF1 measurement (ADS1248 only)
 5 = External REF0 measurement
 6 = AVDD measurement
 7 = DVDD measurement
 
 Register Map (for details see ADS1248 data sheet)
 ============
 00h  MUX0
 01h  VBIAS
 02h  MUX1
 03h  SYS0
 04h  OFC0
 05h  OFC1
 06h  OFC2
 07h  FSC0
 08h  FSC1
 09h  FSC2
 0Ah  IDAC0
 0Bh  IDAC1
 0Ch  GPIOCFG
 0Dh  GPIODIR
 0Eh  GPIODAT
 GP=get Parameters
 return all ADC settings
 _____ end of command description_______________________________________________*/


//*** actual firmware and revision (can be checked with Find_Arduino.vi)

const char firmware[] = "Ardu_24BitADC"; 
const char revision[] = "r3";
//*** will be printed at and of setup

#include <SPI.h>
#include <ADS1248.h>
#include <Streaming.h>

int prescaler [5] = {1, 8, 64, 256, 1024}; //timer prescaler factors 
int i;
char CHAR1 = ' ';
char CHAR2 = ' ';

//create objects
ADS1248 AD;
String text;

//----- interrupt service routine ----
ISR(TIMER1_COMPA_vect) //interval (timer1) elapsed, do measurement
{
  if (CHAR1=='v')  Serial.println(AD.GetVolt() ,9);
  else Serial.println(AD.GetVal());
}

//----- functions -----
boolean SetIntervalTimer (float ti)
{
  int prescale = 1;
  byte i =0;  //timer1 prescaler_reg. value
  unsigned long scaler;
  int sps =5; //adc1248 samples/s 
  float fi; //frequency of interval

  if (ti<0.01) return (false); //sampling interval is to short
  //scaler=(clk*t/prescale)-1
  do
  {
    scaler= (16000000*ti/prescaler[i])-1;
    i++;
  }
  while ((scaler>65535) && (i<5));
  if (scaler>65535) return (false);  //sampling interval is to long
//  //set timer registers
  TCCR1A = 0x00;    // turn on CTC mode
  TCCR1B = 0x08;    // turn on CTC mode
  TCCR1B |= i;      //set prescaler
  OCR1A = scaler;   //load scaer value to compare register
  // set adc sampling rate to max. for selected time interval
  fi=1/ti;
  if (ti<=0.201) do sps=sps*2; 
  while ((sps-1)<=fi);
  AD.SetSamplingRate(sps);
  return (true);
}

boolean ReadADCcont (void)
{ 
  while(Serial.available())(Serial.read()); //clear remaining characters in buffer
  TCNT1 = 0x00;                             //reset timer1 counter register
  TIFR1 = 0xff;                             //clear timer1 interrupt flag register (Note flags are cleared by writing a 1 to it!!!0
  TIMSK1 |= (1 << OCIE1A);                  //enable timer1 compare A interrupt
  Serial.println(F("timer started"));
  while (!Serial.available());              //wait on a character 
  TIMSK1 = 0x00;                            // disable timer1 compare A interrupt
  while(Serial.available())(Serial.read()); //clear remaining characters in buffer
  return (true);
}

//----- configure Arduino ----- 
void setup(void)
{
  pinMode(13, OUTPUT);    
  SetIntervalTimer(1.0);  //Interval default value = 1s
  //Enable Serial
  Serial.begin(57600);
  Serial << firmware << " " << revision << endl;
}

//----- loop = main in C ----- 
void loop()
{
  while (Serial.available()<2); //nothing until at least 2 characters ar in the buffer

  //Command parser 
  delay(5);  //requierd to fetsh optinal parameters
  CHAR1=toupper(Serial.read());
  CHAR2=toupper(Serial.read());
  switch (CHAR1)
  {
  case'S': //CMDstr is a set command
    //--------------------------------
    switch (CHAR2)
    {
    case'C': //set channel,pos,ret,gain
      if (AD.SetCH(Serial.parseInt(),Serial.parseInt(),Serial.parseInt()))Serial.println(F("T"));
      else Serial.println(F("F set channel"));
      break;
    case 'I': //set interval for (continous measurments in seconds)   
      if (SetIntervalTimer(Serial.parseFloat()))   Serial.println(F("T"));
      else Serial.println(F("F interval out of range (0.01s<=t<=4s)"));
      break;
    case'L': //set List(channel0-5 inputs+gain)
      if (AD.SetListCH(Serial.parseInt(),Serial.parseInt(),Serial.parseInt(),Serial.parseInt()))
        Serial.println(F("T"));
      else Serial.println(F("F set list"));
      break;
    case'R': //set register
      if (Serial.available()>2)
      {
        AD.WriteReg(Serial.parseInt(),Serial.parseInt());
        Serial.println(F("T"));
      }  
      else
        Serial.println(F("F set register"));
      break;
    case 'S':  //set Sampling rate
      if (AD.SetSamplingRate(Serial.parseInt()))Serial.println(F("T"));      
      else Serial.println(F("F set sampling"));
      break;

    default:  
      Serial.println(F("F set cmd"));
    }
    break; //Set commands
  case'G': //CMDstr is a get command
    //--------------------------------
    switch (CHAR2)
    {
    case'B': //value of selected channel
      Serial.println(AD.GetVal());		   
      break;  
    case'V': //value of selected channel
      Serial.println(AD.GetVolt(),9);		   
      break;  
    case'C': //read continous selected channel 
      if (Serial.peek() != -1) 
      {
        CHAR1 = tolower(Serial.read());
        switch (CHAR1)
        {
        case'b': //send binary values
        case'v': //send value in volts
          ReadADCcont();
          Serial.println(F("T readADcont stopped"));
          break;
        default:
          Serial.println(F("F selected format for cmd GC not available"));
        }  
      }  
      else Serial.println(F("F output format for cmd GC not selected")); 
      break;  
    case'L': //scan channel list
      if (AD.ScanCH())
      { 
        if (Serial.peek() != -1)  
        {
          text = "";
          CHAR1 = tolower(Serial.read());
          switch (CHAR1)
          {
          case'b': //send binary values
            for (i=0;i<7;i++) text = text + String(i) + "=" + String(AD.CH_VAL[i]) +", ";
            Serial.println (text);
            break;
          case'v': //send value in volts
            AD.CH2Volt();
            for (i=0;i<7;i++) 
            {
              text = String(i) + "=";
              Serial.print (text); 
              Serial.print(AD.CH_VOLT[i],9);
              Serial.print (F(", "));
            }
            Serial.println();
            break;
          default:
            Serial.println(F("F selected format for cmd GL not available"));
          }  
        }  
        else Serial.println(F("F output format for cmd GL not selected")); 
      }
      else   
        Serial.println(F("F no channels to read"));
      break;
    case'R': //read register
      if (Serial.available()>0)
      {
        Serial.println(AD.ReadReg(Serial.parseInt()),HEX);
      }
      break;  
    case'M': //read system monitor     
      if (Serial.available()==1)
      {
        Serial.println(AD.SysMon(Serial.parseInt()));
      }
      break;
    case'P':
      Serial.println(F("F GP cmd not yet implemented"));
      /*
	  sprintf(text, "Input %X", AD.ReadReg(ADS1248_MUX0));
       	  Serial.println (text);
       	  */
      break;
    default: 
      Serial.println(F("F get cmd"));
    } 
    break;//Get commands
  default: 
    Serial.println(F("F cmd"));
  }
  //clear remaining characters in buffer
  while(Serial.available()) Serial.read();
}




















